![Alt text](screencapture-localhost-8080-2018-06-18-08_15_12.png "Title")

# music-tags

> Music Tag and playlist finder

* Using Vue.JS to build different components of the UI
* Bootstrap 4 for layout and FontAwesome 5 for couple of images.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# lint all *.js and *.vue files
npm run lint

# run unit tests
npm test
```

For more information see the [docs for vueify](https://github.com/vuejs/vueify).
